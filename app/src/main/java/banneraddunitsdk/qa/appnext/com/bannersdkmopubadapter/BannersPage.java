package banneraddunitsdk.qa.appnext.com.bannersdkmopubadapter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.appnext.banners.BannerAdRequest;
import com.appnext.sdk.adapters.mopub.banners.AppnextMoPubCustomEventBanner;
import com.mopub.mobileads.MoPubView;

import java.util.HashMap;
import java.util.Map;

import static com.appnext.banners.BannerAdRequest.TYPE_STATIC;
import static com.appnext.banners.BannerAdRequest.VIDEO_LENGTH_LONG;

public class BannersPage extends AppCompatActivity implements View.OnClickListener{
    private MoPubView moPubView;
    private Map<String, Object> extras;
    private Button bannerDefaultLoad;
    private Button bannerCustomLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banners_page);

        moPubView = findViewById(R.id.adview);
        bannerDefaultLoad = findViewById(R.id.loadDefaultBanner);
        bannerDefaultLoad.setOnClickListener(this);

        bannerCustomLoad = findViewById(R.id.loadCustomBanner);
        bannerCustomLoad.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {
        // BANNER
        //moPubView.setAdUnitId("6868416ab5a24ecbae1a64645fa02414");
        //moPubView.setAdUnitId("9f594cab40cf4a1faacbb542b0c525f4");

        switch (v.getId()){
            case (R.id.loadDefaultBanner):
//                if(moPubView != null){
//                    moPubView.destroy();
//                }
                // MEDIUM_RECTANGLE
                moPubView.setAdUnitId("29b78be80696444baf8ae0c2d90bbae6");
                // 6868416ab5a24ecbae1a64645fa02414 new
                // 0470626148cc4bf88f1bc0f9d3483a40 Evgeny
                // 58fec6b4a6d54b8fb7bf0df428d43734 Eden
                // 9f594cab40cf4a1faacbb542b0c525f4
                moPubView.loadAd();
                break;

            case (R.id.loadCustomBanner):
//                if(moPubView != null){
//                    moPubView.destroy();
//                }
                moPubView.setAdUnitId("29b78be80696444baf8ae0c2d90bbae6");
                // 6868416ab5a24ecbae1a64645fa02414 new
                // 0470626148cc4bf88f1bc0f9d3483a40 Evgeny
                // 58fec6b4a6d54b8fb7bf0df428d43734 Eden
                BannerAdRequest config = new BannerAdRequest()
                        .setCategories("omri,leon")
                        .setPostback("Postback string $$$")
//Relevant for MEDIUM_RECTANGLE size only
                        .setCreativeType(TYPE_STATIC)
                        .setAutoPlay(false)
                        .setMute(false)
                        .setVideoLength(VIDEO_LENGTH_LONG)
                        .setClickEnabled(false);
                extras = new HashMap();
                extras.put(AppnextMoPubCustomEventBanner.AppnextConfigurationExtraKey, config);
                moPubView.setLocalExtras(extras);
                moPubView.loadAd();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearMemory();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        clearMemory();
        startActivity(new Intent(BannersPage.this, MainActivity.class));
        finish();

    }

    public void clearMemory(){
        if (moPubView != null) {
            moPubView.destroy();
        }
        if (extras != null)
            extras.clear();
    }
}
