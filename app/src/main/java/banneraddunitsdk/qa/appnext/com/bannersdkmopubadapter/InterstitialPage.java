package banneraddunitsdk.qa.appnext.com.bannersdkmopubadapter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.appnext.ads.fullscreen.FullscreenConfig;
import com.appnext.ads.fullscreen.Video;
import com.appnext.ads.interstitial.Interstitial;
import com.appnext.ads.interstitial.InterstitialConfig;
import com.appnext.core.Ad;
import com.appnext.sdk.adapters.mopub.ads.AppnextMoPubCustomEvent;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubInterstitial;

import java.util.HashMap;
import java.util.Map;

public class InterstitialPage extends AppCompatActivity implements View.OnClickListener{
    private Interstitial interstitialAd;
    private Button loadDefaultInterstitial;
    private Button showDefaultInterstitial;
    private Button loadCustomInterstitial;
    private Button showCustomInterstitial;
    private MoPubInterstitial defaultEventInterstitial;
    private MoPubInterstitial customEventInterstitial;
    private final static String MO_PUB_AD_UNIT_ID = "c9a2b407b9d94aa8805e8229a266301b";
    private InterstitialConfig interstitialConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interstitial_page);

        loadDefaultInterstitial = findViewById(R.id.loadDefaultInterstitial);
        loadDefaultInterstitial.setOnClickListener(this);

        showDefaultInterstitial = findViewById(R.id.showDefaultInterstitial);
        showDefaultInterstitial.setOnClickListener(this);
        showDefaultInterstitial.setEnabled(false);

        loadCustomInterstitial = findViewById(R.id.loadCustomInterstitial);
        loadCustomInterstitial.setOnClickListener(this);

        showCustomInterstitial = findViewById(R.id.showCustomInterstitial);
        showCustomInterstitial.setOnClickListener(this);
        showCustomInterstitial.setEnabled(false);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case (R.id.loadDefaultInterstitial):
                if(defaultEventInterstitial != null){
                    defaultEventInterstitial.destroy();
                }
                defaultEventInterstitial = new MoPubInterstitial(this, MO_PUB_AD_UNIT_ID);
                defaultEventInterstitial.load();
                callBacks(defaultEventInterstitial, "default");
                break;

            case (R.id.showDefaultInterstitial):
                defaultEventInterstitial.show();
                break;

            case (R.id.loadCustomInterstitial):
                if(customEventInterstitial != null){
                    customEventInterstitial.destroy();
                }
                customEventInterstitial = new MoPubInterstitial(this, MO_PUB_AD_UNIT_ID);
                Map<String, Object> extrasInterstitial = new HashMap();
                interstitialConfig = new InterstitialConfig();
                interstitialConfig.setButtonColor("#6AB344");
                interstitialConfig.setPostback("Your_Postback_Params");
                interstitialConfig.setCategories("Categories");
                interstitialConfig.setSkipText("Skip");
                interstitialConfig.setMute(false);
                interstitialConfig.setAutoPlay(true);
                interstitialConfig.setCreativeType(Interstitial.TYPE_MANAGED);
                interstitialConfig.setOrientation(Ad.ORIENTATION_AUTO);
                extrasInterstitial.put(AppnextMoPubCustomEvent.AppnextConfigurationExtraKey, interstitialConfig);
                customEventInterstitial.setLocalExtras(extrasInterstitial);
                customEventInterstitial.load();
                callBacks(customEventInterstitial, "custom");
                break;

            case (R.id.showCustomInterstitial):
                customEventInterstitial.show();
                break;

        }

    }

    public void callBacks(MoPubInterstitial interstitialEvent, final String eventType){
        interstitialEvent.setInterstitialAdListener(new MoPubInterstitial.InterstitialAdListener() {
            @Override
            public void onInterstitialLoaded(MoPubInterstitial interstitial) {
                if(eventType.equals("default")){
                    showDefaultInterstitial.setEnabled(true);
                }else{
                    showCustomInterstitial.setEnabled(true);
                }
                System.out.println("loaded " + eventType + interstitial);

            }

            @Override
            public void onInterstitialFailed(MoPubInterstitial interstitial, MoPubErrorCode errorCode) {
                System.out.println("error "+ errorCode.toString());
            }

            @Override
            public void onInterstitialShown(MoPubInterstitial interstitial) {
                if(eventType.equals("default")){
                    showDefaultInterstitial.setEnabled(false);
                }else{
                    showCustomInterstitial.setEnabled(false);
                }
            }

            @Override
            public void onInterstitialClicked(MoPubInterstitial interstitial) {

            }

            @Override
            public void onInterstitialDismissed(MoPubInterstitial interstitial) {
                System.out.println("dismissed "+ interstitial.toString());

            }
        });


    }

    @Override
    protected void onStop() {
        super.onStop();
        if (defaultEventInterstitial != null) {
            defaultEventInterstitial.destroy();
        }
        if (customEventInterstitial != null) {
            customEventInterstitial.destroy();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (defaultEventInterstitial != null) {
            defaultEventInterstitial.destroy();
        }
        if (customEventInterstitial != null) {
            customEventInterstitial.destroy();
        }
        startActivity(new Intent(InterstitialPage.this, MainActivity.class));
        finish();
    }
}
