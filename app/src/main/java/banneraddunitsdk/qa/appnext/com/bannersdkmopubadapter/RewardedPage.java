package banneraddunitsdk.qa.appnext.com.bannersdkmopubadapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appnext.ads.fullscreen.RewardedConfig;
import com.appnext.ads.fullscreen.RewardedServerSidePostback;
import com.appnext.ads.fullscreen.Video;
import com.appnext.sdk.adapters.mopub.ads.AppnextMoPubCustomEvent;
import com.appnext.sdk.adapters.mopub.ads.AppnextMoPubRewardedVideo;
import com.mopub.common.MoPubReward;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubRewardedVideoListener;
import com.mopub.mobileads.MoPubRewardedVideos;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class RewardedPage extends AppCompatActivity implements View.OnClickListener{

    private Button loadDefaultRewarded;
    private Button showDefaultRewarded;
    private Button loadCustomRewarded;
    private Button showCustomtRewarded;
    private MoPubRewardedVideos moPubVideoDefault = null;
    private MoPubRewardedVideos moPubVideoCustom = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewarded_page);

        loadDefaultRewarded = findViewById(R.id.loadDefaultRewarded);
        loadDefaultRewarded.setOnClickListener(this);

        showDefaultRewarded = findViewById(R.id.showDefaultRewarded);
        showDefaultRewarded.setOnClickListener(this);
        showDefaultRewarded.setEnabled(false);

        loadCustomRewarded = findViewById(R.id.loadCustomRewarded);
        loadCustomRewarded.setOnClickListener(this);

        showCustomtRewarded = findViewById(R.id.showCustomRewarded);
        showCustomtRewarded.setOnClickListener(this);
        showCustomtRewarded.setEnabled(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case (R.id.loadDefaultRewarded):

                moPubVideoDefault.initializeRewardedVideo(this, new AppnextMoPubRewardedVideo.
                        AppnextMediationSettings.Builder()
                        .withRewardsAmountRewarded("asdasd").build());
                moPubVideoDefault.loadRewardedVideo("a7c53e90ed654bada3908b6f71250593"); //appnext pid = e5208600-232f-47b6-bf16-bf8727fae5f0

                callBacks(moPubVideoDefault, "default");
                break;

            case (R.id.showDefaultRewarded):
                moPubVideoDefault.showRewardedVideo("a7c53e90ed654bada3908b6f71250593");

                break;

            case(R.id.loadCustomRewarded):
                moPubVideoCustom.initializeRewardedVideo(this, new AppnextMoPubRewardedVideo.
                        AppnextMediationSettings.Builder()
                        .withRewardsAmountRewarded("asdasd").build());
                RewardedConfig rewardedConfig = new RewardedConfig();
                rewardedConfig.setCategories("Productivity");
                rewardedConfig.setPostback("QA Test Rewarded Mopub");
                rewardedConfig.setMute(true);
                //rewardedConfig.setVideoLength("15");
                rewardedConfig.setOrientation(Video.ORIENTATION_PORTRAIT);
                // rewardedConfig.setMaxVideoLength(40);
                Video.setCacheVideo(true);


                rewardedConfig.setMode("normal");
                rewardedConfig.setMultiTimerLength(15);
                rewardedConfig.setShowCta(true);
                Video.setCacheVideo(false);


                RewardedServerSidePostback postbackParams = new RewardedServerSidePostback("aTran","bUser","cCurr","dAmou","eParam");
                Map<String, Object> extrasRewarded = new HashMap();
                extrasRewarded.put(AppnextMoPubCustomEvent.AppnextConfigurationExtraKey, rewardedConfig);
                extrasRewarded.put(AppnextMoPubCustomEvent.AppnextRewardPostbackExtraKey, postbackParams);
                moPubVideoCustom.loadRewardedVideo("a7c53e90ed654bada3908b6f71250593"); //appnext pid = e5208600-232f-47b6-bf16-bf8727fae5f0
                callBacks(moPubVideoDefault, "custom");
                break;

            case (R.id.showCustomRewarded):
                moPubVideoCustom.showRewardedVideo("a7c53e90ed654bada3908b6f71250593");

                break;

        }
    }

    public void callBacks(MoPubRewardedVideos moPubVideoEvent, final String eventType){

        moPubVideoEvent.setRewardedVideoListener(new MoPubRewardedVideoListener() {
            @Override
            public void onRewardedVideoLoadSuccess(@NonNull String customevent_rewarded_ad_unit_id) {
                Toast.makeText(RewardedPage.this, "Rewarded Loaded", Toast.LENGTH_SHORT).show();
                if(eventType.equals("default")){
                    showDefaultRewarded.setEnabled(true);
                }else{
                    showCustomtRewarded.setEnabled(true);
                }            }

            @Override
            public void onRewardedVideoLoadFailure(@NonNull String customevent_rewarded_ad_unit_id, @NonNull MoPubErrorCode errorCode) {
                Toast.makeText(RewardedPage.this, "Rewarded Load Error " + errorCode.toString(), Toast.LENGTH_SHORT).show();
                if(eventType.equals("default")){
                    showDefaultRewarded.setEnabled(false);
                }else{
                    showCustomtRewarded.setEnabled(false);
                }
            }

            @Override
            public void onRewardedVideoStarted(@NonNull String customevent_rewarded_ad_unit_id) {
                Toast.makeText(RewardedPage.this, "Rewarded Video Started", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onRewardedVideoPlaybackError(@NonNull String customevent_rewarded_ad_unit_id, @NonNull MoPubErrorCode errorCode) {
                Toast.makeText(RewardedPage.this, "Rewarded PlayBack Error " + errorCode.toString(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onRewardedVideoClicked(@NonNull String adUnitId) {
                Toast.makeText(RewardedPage.this, "Rewarded Video Clicked", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onRewardedVideoClosed(@NonNull String customevent_rewarded_ad_unit_id) {
                Toast.makeText(RewardedPage.this, "Rewarded Video CLosed", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onRewardedVideoCompleted(@NonNull Set<String> customevent_rewarded_ad_unit_id, @NonNull MoPubReward reward) {
                Toast.makeText(RewardedPage.this, "Rewarded Video Completed. The Reward is -  " + reward.toString(), Toast.LENGTH_SHORT).show();

                if(eventType.equals("default")){
                    showDefaultRewarded.setEnabled(false);
                }else{
                    showCustomtRewarded.setEnabled(false);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(RewardedPage.this, MainActivity.class));
        finish();
    }
}
