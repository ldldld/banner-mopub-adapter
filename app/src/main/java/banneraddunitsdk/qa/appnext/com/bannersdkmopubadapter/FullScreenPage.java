package banneraddunitsdk.qa.appnext.com.bannersdkmopubadapter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appnext.ads.fullscreen.FullscreenConfig;
import com.appnext.ads.fullscreen.Video;
import com.appnext.core.Ad;
import com.appnext.sdk.adapters.mopub.ads.AppnextMoPubCustomEvent;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubInterstitial;

import java.util.HashMap;
import java.util.Map;

public class FullScreenPage extends AppCompatActivity implements View.OnClickListener{

    private Button showDefaultFullScreen;
    private Button loadDefaultFullScreen;
    private Button showCustomFullScreen;
    private Button loadCustomFullScreen;
    private MoPubInterstitial defaultEventFullScreen;
    private MoPubInterstitial customEventFullScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_page);

        loadDefaultFullScreen = findViewById(R.id.loadDefaultFullScreen);
        loadDefaultFullScreen.setOnClickListener(this);

        showDefaultFullScreen = findViewById(R.id.showDefaultFullScreen);
        showDefaultFullScreen.setOnClickListener(this);
        showDefaultFullScreen.setEnabled(false);


        loadCustomFullScreen = findViewById(R.id.loadCustomFullScreen);
        loadCustomFullScreen.setOnClickListener(this);

        showCustomFullScreen = findViewById(R.id.showCustomFullScreen);
        showCustomFullScreen.setOnClickListener(this);
        showCustomFullScreen.setEnabled(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case (R.id.loadDefaultFullScreen):
                if(defaultEventFullScreen != null){
                    defaultEventFullScreen.destroy();
                }
                defaultEventFullScreen = new MoPubInterstitial(this, "cf0a90e7c0e64c2f9fba9cdea58c6d97");
                defaultEventFullScreen.load();
                callBacks(defaultEventFullScreen, "default");
                break;

            case (R.id.showDefaultFullScreen):
                defaultEventFullScreen.show();

                break;

            case (R.id.loadCustomFullScreen):
                FullscreenConfig fullScreenCustomConfig = new FullscreenConfig();
                fullScreenCustomConfig.setCategories("Action");
                fullScreenCustomConfig.setPostback("YOUR_POSTBACK_HERE");
                fullScreenCustomConfig.setBackButtonCanClose(false);
                fullScreenCustomConfig.setMute(false);
                fullScreenCustomConfig.setVideoLength(Video.VIDEO_LENGTH_LONG);
                fullScreenCustomConfig.setOrientation(Ad.ORIENTATION_AUTO);
                fullScreenCustomConfig.setShowClose(true, 7500);
                customEventFullScreen = new MoPubInterstitial(this, "cf0a90e7c0e64c2f9fba9cdea58c6d97");
                Map<String, Object> fullScreenExtras = new HashMap<>();
                fullScreenExtras.put(AppnextMoPubCustomEvent.AppnextConfigurationExtraKey, fullScreenCustomConfig);
                customEventFullScreen.setLocalExtras(fullScreenExtras);
                customEventFullScreen.load();
                callBacks(customEventFullScreen, "custom");
                break;

            case (R.id.showCustomFullScreen):
                customEventFullScreen.show();
                break;
        }
    }

    public void callBacks(MoPubInterstitial fullScreenEvent, final String eventType ){
        fullScreenEvent.setInterstitialAdListener(new MoPubInterstitial.InterstitialAdListener() {
            @Override
            public void onInterstitialLoaded(MoPubInterstitial interstitial) {
                System.out.println("loaded " + eventType + interstitial);
                if(eventType.equals("default")){
                    showDefaultFullScreen.setEnabled(true);
                }else{
                    showCustomFullScreen.setEnabled(true);
                }

                Toast.makeText(FullScreenPage.this, "Full Screen Loaded", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onInterstitialFailed(MoPubInterstitial interstitial, MoPubErrorCode errorCode) {
                Toast.makeText(FullScreenPage.this, "Error - " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onInterstitialShown(MoPubInterstitial interstitial) {
                if(eventType.equals("default")){
                    showDefaultFullScreen.setEnabled(false);
                }else{
                    showCustomFullScreen.setEnabled(false);
                }
                Toast.makeText(FullScreenPage.this, "Full Screen Shown", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onInterstitialClicked(MoPubInterstitial interstitial) {
                Toast.makeText(FullScreenPage.this, "Full Screen Clicked", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onInterstitialDismissed(MoPubInterstitial interstitial) {

            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (defaultEventFullScreen != null) {
            defaultEventFullScreen.destroy();
        }
        if (customEventFullScreen != null) {
            customEventFullScreen.destroy();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (defaultEventFullScreen != null) {
            defaultEventFullScreen.destroy();
        }
        if (customEventFullScreen != null) {
            customEventFullScreen.destroy();
        }
        startActivity(new Intent(FullScreenPage.this, MainActivity.class));
        finish();
    }
}
