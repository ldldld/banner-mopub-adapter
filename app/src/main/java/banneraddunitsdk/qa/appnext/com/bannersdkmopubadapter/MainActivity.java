package banneraddunitsdk.qa.appnext.com.bannersdkmopubadapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appnext.ads.fullscreen.RewardedConfig;
import com.appnext.ads.fullscreen.RewardedServerSidePostback;
import com.appnext.ads.fullscreen.Video;
import com.appnext.base.Appnext;
import com.appnext.sdk.adapters.mopub.ads.AppnextMoPubCustomEvent;
import com.appnext.sdk.adapters.mopub.ads.AppnextMoPubRewardedVideo;
import com.google.android.gms.ads.InterstitialAd;
import com.mopub.common.MoPubReward;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubRewardedVideoListener;
import com.mopub.mobileads.MoPubRewardedVideos;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button goToBanners;
    private Button goToInterstitial;
    private Button goToFullScreen;
    private Button goToRewarded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        goToBanners = findViewById(R.id.bannersPage);
        goToBanners.setOnClickListener(this);
        goToInterstitial = findViewById(R.id.interstitialPage);
        goToInterstitial.setOnClickListener(this);
        goToFullScreen = findViewById(R.id.fullScreenPage);
        goToFullScreen.setOnClickListener(this);
        goToRewarded = findViewById(R.id.rewardedPage);
        goToRewarded.setOnClickListener(this);
        Appnext.init(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case (R.id.bannersPage):
                startActivity(new Intent(MainActivity.this, BannersPage.class));
                finish();
                break;

            case (R.id.interstitialPage):
                startActivity(new Intent(MainActivity.this, InterstitialPage.class));
                finish();
                break;

            case (R.id.fullScreenPage):
                startActivity(new Intent(MainActivity.this, FullScreenPage.class));
                finish();
                break;

            case(R.id.rewardedPage):
                startActivity(new Intent(MainActivity.this, RewardedPage.class));
                finish();
                break;



        }
    }



}
